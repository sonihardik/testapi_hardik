This is API testing automation project which has been done using RestAssured and JUnit.
Automation Flows which are covered:

1.  User Category
	 1. POST HTTP Method by passing valid values.
1.  Todo Category
	1. POST HTTP Method by passing valid values.
1.  POST Category
	1. POST HTTP Method by passing valid values.
1.  Photos Category
	1. POST HTTP Method by passing valid values.
1.  Comments Category
	1. GET HTTP Method by passing valid values.
1.  Album Category
	1. Put HTTP Method by passing valid values.	