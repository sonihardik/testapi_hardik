package com.opus.test.APITest;

import java.util.HashMap;
import java.util.Map;

import io.restassured.RestAssured;
import io.restassured.response.Response;

/**
 * This class will be used to do all the HTTP operations.
 * 
 * @author Hardik Soni
 *
 */
public class HTTPCleint extends TestDataInitialization {

	/**
	 * This method is used to perform the HTTP Post operation.
	 * 
	 * @param Body
	 * @param URL
	 * @return
	 */
	public Response postRequest(Object Body, String URL) {

		Response response = RestAssured.given().contentType("application/json").headers(createCommonHeaders())
				.relaxedHTTPSValidation().body(Body).when().log().all().post(URL);
		return response;
	}

	/**
	 * This method is used to perform the HTTP Put operation.
	 * 
	 * @param Body
	 * @param URL
	 * @return
	 */
	public Response putRequest(Object Body, String URL) {

		Response response = RestAssured.given().contentType("application/json").headers(createCommonHeaders())
				.relaxedHTTPSValidation().body(Body).when().log().all().put(URL);
		return response;
	}

	/**
	 * This method will be used to perform the HTTP Get operation.
	 * 
	 * @param URL
	 * @return
	 */
	public Response getRequest(String URL) {

		Response response = RestAssured.given().contentType("application/json").headers(createCommonHeaders())
				.relaxedHTTPSValidation().when().log().all().get(URL);
		return response;
	}

	/**
	 * Common HTTP headers values
	 * 
	 * @return
	 */
	public Map<String, String> createCommonHeaders() {
		Map<String, String> commonHeaders = new HashMap<String, String>();
		commonHeaders.put("Content-Type", "application/json; charset=UTF-8");
		commonHeaders.put("Accept", "application/json");
		return commonHeaders;
	}

}
