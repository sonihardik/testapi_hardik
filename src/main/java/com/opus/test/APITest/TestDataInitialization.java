package com.opus.test.APITest;

import io.restassured.RestAssured;

/**
 * This class will be used to maintane all the end points(ULRs).
 * @author Hardik Soni
 *
 */
public class TestDataInitialization {

	public String postCategoryURL,commentsCategoryURL, albumCategoryURL, photosCategoryURL, userCategoryURL, todoCategoryURL;
	public static String locationResponseHeader = "location";

	/**
	 * No-arg constructor override to setup the URL.
	 * 
	 */
	public TestDataInitialization() {

		// Setting up the Host URLs
		
		RestAssured.baseURI = "http://ec2-54-174-213-136.compute-1.amazonaws.com:3000";
		postCategoryURL = "/posts";
		commentsCategoryURL= "/comments";
		albumCategoryURL = "/albums";
		photosCategoryURL = "/photos";
		userCategoryURL = "/users";
		todoCategoryURL = "todos";

	}
	
	/**
	 * Method to create run time URL for COMMNETS category section.
	 * i.e. "/comments/12333"
	 * @param id
	 * @return
	 */
	public String createCommentsURLWithId(String id) {
		return commentsCategoryURL + "/" + id;
	}
	
	/**
	 * Method to create run time URL for ALBUM category section.
	 * i.e. "/albums/12333"
	 * @param id
	 * @return
	 */
	public String createAlbumURLWithId(long id) {
		return albumCategoryURL + "/" + id;
	} 

}