package com.opus.test.common;

import lombok.Data;

/**
 * Request/Response class for the ADDRESS category
 * 
 * @author Hardik Soni
 *
 */
@Data
public class Address {

	private String street;
	private String suite;
	private String city;
	private String zipcode;
	private Geo geo = new Geo();

	/**
	 * No-arg constructor override to setup initial values.
	 */
	public Address() {
		street = "Kulas Light";
		city = "Gwenborough";
		zipcode = "92998-3874";
	}
}
