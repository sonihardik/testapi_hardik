package com.opus.test.common;

import lombok.Data;

/**
 * Request/Response class used for the Company object
 * @author Hardik Soni
 *
 */
@Data
public class Company {
	private String name;
	private String catchPhrase;
	private String bs;
	
	/**
	 * No-arg constructor override to setup the initial values.
	 */
	public Company() {
		bs = "harness real-time e-markets";
		name = "Romaguera-Crona";
		catchPhrase = "Multi-layered client-server neural-net";
	}
}

