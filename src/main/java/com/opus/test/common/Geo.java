package com.opus.test.common;

import lombok.Data;

/**
 * Request/Response value used for the Geo class.
 * 
 * @author Hardik Soni
 */
@Data
public class Geo {
	private String lat;
	private String lng;

	/**
	 * No-arg constructor override to setup the initial values.
	 */
	public Geo() {
		lat = "-37.3159";
		lng = "81.1496";
	}
}
