package com.opus.test.request;

import lombok.Data;

/**
 * Request DTO for Album category
 * @author Hardik Soni
 *
 */
@Data
public class AlbumRequest {
	private long userId;
	private String title;
}
