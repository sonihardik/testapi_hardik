package com.opus.test.request;

import lombok.Data;

/**
 * Request DTO for Comments category
 * @author Hardik Soni
 *
 */
@Data
public class CommentsRequest {

	private int postId;
	private String name;
	private String email;
	private String body;
}
