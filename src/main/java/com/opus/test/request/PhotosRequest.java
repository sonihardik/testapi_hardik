package com.opus.test.request;

import lombok.Data;

/**
 * Request DTO for Photos category
 * @author Hardik Soni
 *
 */
@Data
public class PhotosRequest {

	private long postId;
	private String title;
	private String url;
	private String thumbnailUrl;
}
