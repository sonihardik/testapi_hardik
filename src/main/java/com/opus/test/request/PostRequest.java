package com.opus.test.request;

import lombok.Data;

/**
 * Request DTO for POST category
 * @author Hardik SOni
 *
 */
@Data
public class PostRequest {
	private long userId;
	private String title;
	private String body;
}
