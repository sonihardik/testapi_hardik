package com.opus.test.request;

import lombok.Data;

/**
 * Request DTO for TODO category.
 * 
 * @author Hardik Soni
 *
 */
@Data
public class TodoRequest {

	private long userId;
	private String title;
	private boolean completed;
}
