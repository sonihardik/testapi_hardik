package com.opus.test.request;

import org.apache.commons.lang3.RandomStringUtils;

import com.opus.test.common.Address;
import com.opus.test.common.Company;

import lombok.Data;

/**
 * Request DTO for User category.
 * 
 * @author Hardik Soni
 *
 */
@Data
public class UserRequest {

	private String name;
	private String username;
	private String email;
	private String phone;
	private String website;
	private Address address = new Address();
	private Company company = new Company();

	/**
	 * No-arg constuctor to define the intial values.
	 */
	public UserRequest() {
		name = "Leanne Graham";
		username = RandomStringUtils.randomAlphabetic(10);
		email = RandomStringUtils.randomAlphabetic(10) + "@april.biz";
		phone = "1-770-736-8031 x56442";
		website = "hildegard.org";
	}
}
