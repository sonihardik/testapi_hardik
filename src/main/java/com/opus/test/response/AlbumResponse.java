package com.opus.test.response;

import lombok.Data;

/**
 * DTO Response for Album category
 * 
 * @author Hardik Soni
 *
 */
@Data
public class AlbumResponse {
	private int userId;
	private long id;
	private String title;
}
