package com.opus.test.response;

import lombok.Data;

/**
 * DTO response for Comments category
 * 
 * @author Hardik Soni
 *
 */
@Data
public class CommentsResponse {

	private int postId;
	private String id;
	private String name;
	private String email;
	private String body;
}
