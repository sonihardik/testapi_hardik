package com.opus.test.response;

import lombok.Data;

/**
 * DT response for Photos category
 * 
 * @author Hardik Soni
 *
 */
@Data
public class PhotosResponse {

	private long postId;
	private long id;
	private String title;
	private String url;
	private String thumbnailUrl;
}
