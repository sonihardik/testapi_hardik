package com.opus.test.response;

import lombok.Data;

/**
 * DTO response for POST category
 * 
 * @author Hardik Soni
 *
 */
@Data
public class PostResponse {
	private int userId;
	private String id;
	private String title;
	private String body;
}
