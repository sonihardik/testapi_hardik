package com.opus.test.response;

import lombok.Data;

/**
 * DTO response for TODO category
 * 
 * @author Hardik Soni
 *
 */
@Data
public class TodoResponse {

	private long userId;
	private long id;
	private String title;
	private boolean completed;
}
