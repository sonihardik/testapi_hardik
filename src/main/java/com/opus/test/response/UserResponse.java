package com.opus.test.response;

import com.opus.test.common.Address;
import com.opus.test.common.Company;

import lombok.Data;
/**
 * DTO response for User category
 * @author Hardik Soni
 *
 */
@Data
public class UserResponse {

	private String name;
	private String username;
	private String email;
	private String phone;
	private String website;
	private Address address;
	private Company company;
	private long id;
}
