package com.opus.test.APITest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opus.test.request.AlbumRequest;
import com.opus.test.request.UserRequest;
import com.opus.test.response.AlbumResponse;
import com.opus.test.response.UserResponse;

import io.restassured.response.Response;

/**
 * This class will be used to Test Album category
 * 
 * @author Hardik Soni
 *
 */
public class AlbumCategoryPutHttpMethodTest extends HTTPCleint {

	Gson gson = new GsonBuilder().create();
	AlbumRequest albumRequest = new AlbumRequest();
	Random rand = new Random();
	String headerLocationValue;
	long id;

	/**
	 * Before method to put data into Album category. i.e. UserId,Title
	 * 
	 * Then all the values should come out as output.
	 */
	@Before
	public void beforeTestAlbumCategoryByPassingUserIdAndTitleUsingPOSTMethodShouldSuccess() {
		
		// Create User
		UserRequest userRequest = new UserRequest();
		Response userResponse = postRequest(userRequest, userCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + userResponse.getStatusCode(), 201,
				userResponse.getStatusCode());
		UserResponse userResponseClass = gson.fromJson(userResponse.prettyPrint(), UserResponse.class);
		assertNotNull(userResponseClass.getId());
		
		
		// Create Album

		albumRequest.setUserId(userResponseClass.getId());
		albumRequest.setTitle("Creating Album for the PUT method test");

		Response response = postRequest(albumRequest, albumCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());

		// Assert Response Body
		AlbumResponse albumResponse = gson.fromJson(response.print(), AlbumResponse.class);

		assertEquals("Expected UserId for the Album section was " + albumRequest.getUserId() + " But coming as "
				+ albumResponse.getUserId(), albumRequest.getUserId(), albumResponse.getUserId());

		assertEquals("Expected Title for the Album section was " + albumRequest.getTitle() + " But coming as "
				+ albumResponse.getTitle(), albumRequest.getTitle(), albumResponse.getTitle());

		assertNotNull(albumResponse.getId());
		id = albumResponse.getId();

		// Assert Response Header and fetch value for 'location'
		headerLocationValue = response.getHeader(locationResponseHeader);

		assertNotNull(headerLocationValue);
		assertThat(headerLocationValue, containsString(String.valueOf(id)));
	}

	/**
	 * Test method to test http Put method.
	 * 
	 * Then all the value should be return which are there in Album category.
	 */
	@Test
	public void testAlbumCategoryUsingPutMethodShouldSuccess() {

		AlbumRequest albumRequestForPut = new AlbumRequest();

		albumRequestForPut.setUserId(albumRequest.getUserId());
		albumRequestForPut.setTitle("Update Album for the PUT method test");

		Response response = putRequest(albumRequestForPut, createAlbumURLWithId(id));

		// Assert Status code

		assertEquals("Expected HTTP status code was " + 200 + "but coming as " + response.getStatusCode(), 200,
				response.getStatusCode());

		// Assert Response Body
		AlbumResponse albumResponse = gson.fromJson(response.print(), AlbumResponse.class);

		assertEquals("Expected UserId for the Album section was " + albumRequestForPut.getUserId() + " But coming as "
				+ albumResponse.getUserId(), albumRequestForPut.getUserId(), albumResponse.getUserId());

		assertEquals("Expected Title for the Album section was " + albumRequestForPut.getTitle() + " But coming as "
				+ albumResponse.getTitle(), albumRequestForPut.getTitle(), albumResponse.getTitle());

		assertNotEquals(
				"Expected Title for the Album should be updated and should not be same, but coming as same, as "
						+ albumRequest.getTitle() + "And" + albumRequestForPut.getTitle(),
				albumRequest.getTitle(), albumRequestForPut.getTitle());

	}
}
