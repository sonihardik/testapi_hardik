package com.opus.test.APITest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opus.test.request.CommentsRequest;
import com.opus.test.response.CommentsResponse;

import io.restassured.response.Response;

/**
 * This class will be used to Test COMMENTS category
 * 
 * @author Hardik Soni
 *
 */
public class CommentsCategoryGETHttpMethodTest extends HTTPCleint {

	Gson gson = new GsonBuilder().create();
	CommentsRequest commentsRequest = new CommentsRequest();
	Random rand = new Random();
	String headerLocationValue, id;

	/**
	 * Before method to put data into COMMENTS category. i.e. Title, Body,UserId,
	 * Id(as Integer)
	 * 
	 * Then all the values should come out as output.
	 */
	@Before
	public void beforeTestCOMMENTSCategoryByPassingIDUsingPOSTMethodShouldSuccess() {

		commentsRequest.setPostId(rand.nextInt(1000000));
		commentsRequest.setName("Test Comments Addition");
		commentsRequest.setEmail("test@test.com");
		commentsRequest
				.setBody("Here we are adding comment section which we will use for getting values using GET method");

		Response response = postRequest(commentsRequest, commentsCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());

		// Assert Response Body
		CommentsResponse commentsResponse = gson.fromJson(response.print(), CommentsResponse.class);

		assertEquals("Expected PostId for the COMMENTS section was " + commentsRequest.getPostId() + " But coming as "
				+ commentsResponse.getPostId(), commentsRequest.getPostId(), commentsResponse.getPostId());

		assertNotNull(commentsResponse.getId());
		id = commentsResponse.getId();

		assertEquals("Expected Name for the COMMENTS section was " + commentsRequest.getName() + " But coming as "
				+ commentsResponse.getName(), commentsRequest.getName(), commentsResponse.getName());

		assertEquals("Expected Email for the COMMENTS section was " + commentsRequest.getEmail() + " But coming as "
				+ commentsResponse.getEmail(), commentsRequest.getEmail(), commentsResponse.getEmail());

		// Assert Response Header and fetch value for 'location'
		headerLocationValue = response.getHeader(locationResponseHeader);

		assertNotNull(headerLocationValue);
		assertThat(headerLocationValue, containsString(commentsResponse.getId().toString()));
	}

	/**
	 * Test method to test http GET method.
	 * 
	 * Then all the value should be return which are there in COMMENTS category.
	 */
	@Test
	public void testCOMMENTSCategoryUsingGETMethodShouldSuccess() {

		Response response = getRequest(createCommentsURLWithId(id));

		// Assert Status code

		assertEquals("Expected HTTP status code was " + 200 + "but coming as " + response.getStatusCode(), 200,
				response.getStatusCode());

		// Assert Response Body
		CommentsResponse commentsResponse = gson.fromJson(response.print(), CommentsResponse.class);

		assertEquals("Expected PostId for the COMMENTS section was " + commentsRequest.getPostId() + " But coming as "
				+ commentsResponse.getPostId(), commentsRequest.getPostId(), commentsResponse.getPostId());

		assertEquals("Expected Id for the COMMENTS section was " + id + " But coming as " + commentsResponse.getId(),
				id, commentsResponse.getId());

		assertEquals("Expected Name for the COMMENTS section was " + commentsRequest.getName() + " But coming as "
				+ commentsResponse.getName(), commentsRequest.getName(), commentsResponse.getName());

		assertEquals("Expected Email for the COMMENTS section was " + commentsRequest.getEmail() + " But coming as "
				+ commentsResponse.getEmail(), commentsRequest.getEmail(), commentsResponse.getEmail());

	}
}
