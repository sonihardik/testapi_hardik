package com.opus.test.APITest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opus.test.request.AlbumRequest;
import com.opus.test.request.PhotosRequest;
import com.opus.test.request.UserRequest;
import com.opus.test.response.AlbumResponse;
import com.opus.test.response.PhotosResponse;
import com.opus.test.response.UserResponse;

import io.restassured.response.Response;

/**
 * This class will be used to Test Photos category
 * 
 * @author Hardik Soni
 *
 */
public class PhotosCategoryPostHttpMethodTest extends HTTPCleint {

	Gson gson = new GsonBuilder().create();
	AlbumRequest albumRequest = new AlbumRequest();
	Random rand = new Random();
	String headerLocationValue;
	long id;

	/**
	 * Before method to put data into Album category. i.e. UserId,Title
	 * 
	 * Then all the values should come out as output.
	 */
	@Before
	public void beforeTestAlbumCategoryByPassingUserIdAndTitleUsingPOSTMethodShouldSuccess() {

		// Create User
		UserRequest userRequest = new UserRequest();
		Response userResponse = postRequest(userRequest, userCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + userResponse.getStatusCode(), 201,
				userResponse.getStatusCode());
		UserResponse userResponseClass = gson.fromJson(userResponse.prettyPrint(), UserResponse.class);
		assertNotNull(userResponseClass.getId());

		// Create Album
		albumRequest.setUserId(userResponseClass.getId());
		albumRequest.setTitle("Creating Album for the PUT method test");

		Response response = postRequest(albumRequest, albumCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());

		// Assert Response Body
		AlbumResponse albumResponse = gson.fromJson(response.print(), AlbumResponse.class);

		assertEquals("Expected UserId for the Album section was " + albumRequest.getUserId() + " But coming as "
				+ albumResponse.getUserId(), albumRequest.getUserId(), albumResponse.getUserId());

		assertEquals("Expected Title for the Album section was " + albumRequest.getTitle() + " But coming as "
				+ albumResponse.getTitle(), albumRequest.getTitle(), albumResponse.getTitle());

		assertNotNull(albumResponse.getId());
		id = albumResponse.getId();
	}

	/**
	 * Test method to test http Post method.
	 * 
	 * Then all the value should be return which are there in Photos category.
	 */
	@Test
	public void testPhotosCategoryUsingPostMethodShouldSuccess() {

		PhotosRequest photosRequest = new PhotosRequest();
		photosRequest.setPostId(id);
		photosRequest.setThumbnailUrl("http://placehold.it/150/92c952");
		photosRequest.setTitle("accusamus beatae ad facilis cum similique qui sunt");
		photosRequest.setUrl("http://placehold.it/600/92c952");

		Response response = postRequest(photosRequest, photosCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());
		// Assert Response Body
		PhotosResponse photosResponse = gson.fromJson(response.print(), PhotosResponse.class);
		assertEquals(
				"Expected PostId was " + photosRequest.getPostId() + " But coming as " + photosResponse.getPostId(),
				photosRequest.getPostId(), photosResponse.getPostId());
		assertEquals(
				"Expected ThumnailUrl was " + photosRequest.getThumbnailUrl() + "But coming as "
						+ photosResponse.getThumbnailUrl(),
				photosRequest.getThumbnailUrl(), photosResponse.getThumbnailUrl());
		assertEquals("Expected Title was " + photosRequest.getTitle() + "But coming as " + photosResponse.getTitle(),
				photosRequest.getTitle(), photosResponse.getTitle());
		assertEquals("Expecrted URL was " + photosRequest.getUrl() + "But coming as  " + photosResponse.getUrl(),
				photosRequest.getUrl(), photosResponse.getUrl());
		assertNotNull(photosResponse.getId());

	}
}
