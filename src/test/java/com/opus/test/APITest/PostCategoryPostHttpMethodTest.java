package com.opus.test.APITest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opus.test.request.PostRequest;
import com.opus.test.request.UserRequest;
import com.opus.test.response.PostResponse;
import com.opus.test.response.UserResponse;

import io.restassured.response.Response;

/**
 * This class will be used to Test POST category 
 * @author Hardik Soni
 *
 */
public class PostCategoryPostHttpMethodTest extends HTTPCleint {

	Gson gson = new GsonBuilder().create();
	Random rand = new Random();
	long userId;

	
	/**
	 * Create User whose userId will pass into Post categiory
	 */
	
	@Before
	public void createUserShouldSuccess() {
		UserRequest userRequest = new UserRequest();
		
		Response response = postRequest(userRequest, userCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());
		UserResponse userResponse = gson.fromJson(response.prettyPrint(), UserResponse.class);
		assertNotNull(userResponse.getId());
		userId = userResponse.getId();
		
	}
	
	/**
	 * Test method to test http POST method by passing all the values..
	 * i.e. Title, Body,UserId, Id(as Integer)
	 * 
	 * Then all the values should come out as output.
	 */

	@Test
	public void testPOSTCategoryByPassingIDUsingPOSTMethodShouldSuccess() {
		
		PostRequest postRequest = new PostRequest();
		postRequest.setTitle("Testing POST category");
		postRequest.setBody("Testing POST category using POST HTTP method by passing random UserId");
		postRequest.setUserId(userId);

		Response response = postRequest(postRequest, postCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());
		
		// Assert Response body
		PostResponse postResponse = gson.fromJson(response.prettyPrint(), PostResponse.class);
		assertEquals("Expected Title for the Post request was = " + postRequest.getTitle() + " but coming as "
				+ postResponse.getTitle(), postRequest.getTitle(), postResponse.getTitle());
		assertEquals("Expected Body for the Post request was = " + postRequest.getBody() + " but coming as "
				+ postResponse.getBody(), postRequest.getBody(), postResponse.getBody());
		
		assertEquals("Expected UserId for the Post request was = " + postRequest.getUserId() + " but coming as "
				+ postResponse.getUserId(), postRequest.getUserId(), postResponse.getUserId());
		assertNotNull(postResponse.getId());
		
		// Assert Response Header and fetch value for 'location'
		String headerLocationValue = response.getHeader(locationResponseHeader);
		
		assertNotNull(headerLocationValue);
		assertThat(headerLocationValue, containsString(postResponse.getId().toString()));
	}
}
