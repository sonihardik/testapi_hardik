package com.opus.test.APITest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opus.test.request.TodoRequest;
import com.opus.test.request.UserRequest;
import com.opus.test.response.TodoResponse;
import com.opus.test.response.UserResponse;

import io.restassured.response.Response;

/**
 * This Test class will be used to test the TODO category
 * @author Hardik Soni
 *
 */
public class TodocategoryPostHttpMethodTest extends HTTPCleint {

	Gson gson = new GsonBuilder().create();
	Random rand = new Random();
	long userId;

	/**
	 * Create User whose userId will pass into Post category
	 */

	@Before
	public void createUserShouldSuccess() {
		UserRequest userRequest = new UserRequest();

		Response response = postRequest(userRequest, userCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());
		UserResponse userResponse = gson.fromJson(response.prettyPrint(), UserResponse.class);
		assertNotNull(userResponse.getId());
		userId = userResponse.getId();
	}

	/**
	 * Test method to test http Todo method by passing all the values..
	 * 
	 * Then all the values should come out as output.
	 */

	@Test
	public void testTodoCategoryByPassingValidValuesPOSTMethodShouldSuccess() {
		TodoRequest todoRequest = new TodoRequest();
		todoRequest.setTitle("delectus aut autem");
		todoRequest.setUserId(userId);
		todoRequest.setCompleted(true);

		Response response = postRequest(todoRequest, todoCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());
		TodoResponse todoResponse = gson.fromJson(response.prettyPrint(), TodoResponse.class);

		assertEquals("Expected Title was " + todoRequest.getTitle() + " But coming as " + todoResponse.getTitle(),
				todoRequest.getTitle(), todoResponse.getTitle());
		assertEquals("Expected userId was " + todoRequest.getUserId() + "But coming as " + todoResponse.getUserId(),
				todoRequest.getUserId(), todoResponse.getUserId());

		assertNotNull(todoResponse.getId());

		// Assert Response Header and fetch value for 'location'
		String headerLocationValue = response.getHeader(locationResponseHeader);

		assertNotNull(headerLocationValue);
		assertThat(headerLocationValue, containsString(String.valueOf(todoResponse.getId())));
	}

}
