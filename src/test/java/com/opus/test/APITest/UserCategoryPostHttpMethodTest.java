package com.opus.test.APITest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Random;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opus.test.common.Address;
import com.opus.test.common.Company;
import com.opus.test.request.UserRequest;
import com.opus.test.response.UserResponse;

import io.restassured.response.Response;

/**
 * This class will be used to Test USER category
 * 
 * @author Hardik Soni
 *
 */
public class UserCategoryPostHttpMethodTest extends HTTPCleint {

	Gson gson = new GsonBuilder().create();
	Random rand = new Random();

	/**
	 * Test method to test http User method by passing all the values..
	 * 
	 * Then all the values should come out as output.
	 */

	@Test
	public void testUserCategoryByPassingValidValuesPOSTMethodShouldSuccess() {

		UserRequest userRequest = new UserRequest();

		Response response = postRequest(userRequest, userCategoryURL);

		// Assert Status code
		assertEquals("Expected HTTP status code was " + 201 + "but coming as " + response.getStatusCode(), 201,
				response.getStatusCode());

		// Assert Response body

		UserResponse userResponse = gson.fromJson(response.prettyPrint(), UserResponse.class);

		assertEquals("Expected Name was" + userRequest.getName() + "But coming as " + userResponse.getName(),
				userRequest.getName(), userResponse.getName());
		assertEquals(
				"Expected UserName was " + userRequest.getUsername() + " But coming as " + userResponse.getUsername(),
				userRequest.getUsername(), userResponse.getUsername());
		assertEquals("Expected PhoneNumber was" + userRequest.getPhone() + " but coming as " + userResponse.getPhone(),
				userRequest.getPhone(), userResponse.getPhone());
		assertEquals("Expected WebSite was " + userRequest.getWebsite() + " But coming as " + userResponse.getWebsite(),
				userRequest.getWebsite(), userResponse.getWebsite());

		Address addressRequest = userRequest.getAddress();
		Address addressResponse = userResponse.getAddress();
		
		assertEquals("Expected address.street was " + addressRequest.getStreet() + " But coming as " + addressResponse.getStreet(),
				addressRequest.getStreet(), addressResponse.getStreet());
		
		assertEquals("Expected address.suite was " + addressRequest.getSuite() + " But coming as " + addressResponse.getSuite(),
				addressRequest.getSuite(), addressResponse.getSuite());
		
		assertEquals("Expected address.city was " + addressRequest.getCity() + " But coming as " + addressResponse.getCity(),
				addressRequest.getCity(), addressResponse.getCity());
		
		assertEquals("Expected address.zipcode was " + addressRequest.getZipcode() + " But coming as " + addressResponse.getZipcode(),
				addressRequest.getZipcode(), addressResponse.getZipcode());
		
		assertEquals("Expected address.geo.lat was " + addressRequest.getGeo().getLat() + " But coming as " + addressResponse.getGeo().getLat(),
				addressRequest.getGeo().getLat(), addressResponse.getGeo().getLat());
		
		assertEquals("Expected address.geo.lng was " + addressRequest.getGeo().getLng() + " But coming as " + addressResponse.getGeo().getLng(),
				addressRequest.getGeo().getLng(), addressResponse.getGeo().getLng());
		
		Company requestCompnay = userRequest.getCompany();
		Company responseCompnay = userResponse.getCompany();

		assertEquals("Expected company.name was " + requestCompnay.getBs() + " But coming as " + responseCompnay.getBs(),
				requestCompnay.getBs(), responseCompnay.getBs());
		
		assertEquals("Expected company.catchPhrase was " + requestCompnay.getCatchPhrase() + " But coming as " + responseCompnay.getCatchPhrase(),
				requestCompnay.getCatchPhrase(), responseCompnay.getCatchPhrase());
		
		assertEquals("Expected company.bs was " + requestCompnay.getBs() + " But coming as " + responseCompnay.getBs(),
				requestCompnay.getBs(), responseCompnay.getBs());
		
		// Assert Response Header and fetch value for 'location'
		String headerLocationValue = response.getHeader(locationResponseHeader);

		assertNotNull(headerLocationValue);
		assertThat(headerLocationValue, containsString(String.valueOf(userResponse.getId())));
	}
}
